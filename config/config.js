module.exports = {
    port: 8080,
    mongoUrl: 'mongodb://localhost:27017/twitter-bot',

    twitterConfig : {
        consumer_key: process.env.CONSUMER_KEY,
        consumer_secret: process.env.CONSUMER_SECRET,
        access_token_key: process.env.ACCESS_TOKEN_KEY,
        access_token_secret: process.env.ACCESS_TOKEN_SECRET,
    }
}